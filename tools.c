#include "tools.h"
#include "fonts.h"


#define BASE_FREQUENCY 84000000

#define CAPTURE_PRESCALER 1
#define CAPTURE_CORRECTION 1

#define COUNTER_PRESCALER 1
#define COUNTER_CORRECTION 1
#define COUNTER_INTERVAL 20

#define LIN_LENGTH 4

volatile uint32_t ms;
bool clearDisplayExitFlag;
volatile uint32_t frequency;
volatile enum : uint8_t {
	CAPTURE_MODE,
	COUNTER_MODE
} frequencyMode;
volatile uint16_t keys;
bool adcExitFlag;


// declaration of static functions

static inline void dataToReg(uint16_t *data, uint16_t *reg, uint8_t fromData, uint8_t fromReg, uint8_t size);
static inline uint8_t checksum(const uint8_t bytes[], uint8_t n, uint8_t id);
static void FrequencyInit();
static inline void LCD_Output16BitWord(uint16_t data);
static inline void LCD_WriteCommand(uint16_t command);
static inline void LCD_WriteData(uint16_t data);
static inline void LCD_WriteReg(uint16_t command, uint16_t data);


// helper functions

// general function to copy sections of bitfields
static inline void dataToReg(uint16_t *data, uint16_t *reg, uint8_t fromData, uint8_t fromReg, uint8_t size) {

  // generate bitmasks (dataMask to be applied to the input data, regMask to be applied to the output data)
  const uint16_t dataMask = (((1 << size) - 1) << fromData);
  const uint16_t regMask = ~(((1 << size) - 1) << fromReg);

  uint16_t tmp;

  // decide on the shift direction
  if (fromData > fromReg) { 
    tmp = ((*data & dataMask) >> (fromData - fromReg));
  } else {
    tmp = ((*data & dataMask) << (fromReg - fromData));
  }

  // write to the output register
  *reg = (*reg & regMask) | tmp;
}

static inline uint8_t checksum(const uint8_t bytes[], uint8_t n, uint8_t id) {

  // iterate through the bytes
  for (int i = 0; i <= n; i++) {

    // add the bytes value onto the current sum (we use the id int as the sum)
    uint8_t tmp = id + bytes[i];

    // check if we had an overflow
    if (tmp < id) {

      // an overflow accured, add a one as well
      id = tmp + 1;
    } else {

      // no overflow accured, just assign to the sum
      id = tmp;
    }
  }
  
  // return the bitflipped sum
  return ~id;
}



// public functions


// initialize the GPIO ports and pins
void LCDInit() {
	
	// enable GPIO A, B, D and E ports
	RCC->AHB1ENR |= RCC_AHB1ENR_GPIOAEN | RCC_AHB1ENR_GPIOBEN | RCC_AHB1ENR_GPIODEN | RCC_AHB1ENR_GPIOEEN;
	
	// GPIOA
	{
	
		// pin 0 is already configured as an input
		
	}
	
	// GPIOB
	{
	
		// set the required pins as gerneral purpose output
		GPIOB->MODER &= ~((3 << (8 * 2)) | (3 << (9 * 2)));
		GPIOB->MODER |= (1 << (8 * 2)) | (1 << (9 * 2));
		
		// no need to set the OTYPER register, as it is set to push/pull on reset
		
		// enable pull up resistors for the required pins
		GPIOB->PUPDR &= ~((3 << (8 * 2)) | (3 << (9 * 2)));
		GPIOB->PUPDR |= (1 << (8 * 2)) | (1 << (9 * 2));
		
		// the SPEEDER register is irrelevant
		
	}
	
	// GPIOD
	{
	
		// set the required pins as gerneral purpose output
		GPIOD->MODER &= ~((3 << (0 * 2)) | (3 << (1 * 2)) | (3 << (3 * 2))  | (3 << (4 * 2)) | (3 << (5 * 2)) | (3 << (7 * 2)) | (3 << (8 * 2)) | (3 << (9 * 2)) | (3 << (10 * 2)) | (3 << (11 * 2)) | (3 << (12 * 2)) | (3 << (13 * 2)) | (3 << (14 * 2)) | (3 << (15 * 2)));
		GPIOD->MODER |= (1 << (0 * 2)) | (1 << (1 * 2)) | (1 << (3 * 2))  | (1 << (4 * 2)) | (1 << (5 * 2)) | (1 << (7 * 2)) | (1 << (8 * 2)) | (1 << (9 * 2)) | (1 << (10 * 2)) | (1 << (11 * 2)) | (1 << (12 * 2)) | (1 << (13 * 2)) | (1 << (14 * 2)) | (1 << (15 * 2));
		
		// no need to set the OTYPER register, as it is set to push/pull on reset
		
		// enable pull up resistors for the required pins
		GPIOD->PUPDR &= ~((3 << (0 * 2)) | (3 << (1 * 2)) | (3 << (3 * 2))  | (3 << (4 * 2)) | (3 << (5 * 2)) | (3 << (7 * 2)) | (3 << (8 * 2)) | (3 << (9 * 2)) | (3 << (10 * 2)) | (3 << (11 * 2)) | (3 << (13 * 2)) | (3 << (14 * 2)) | (3 << (15 * 2)));
		GPIOD->PUPDR |= (1 << (0 * 2)) | (1 << (1 * 2)) | (1 << (3 * 2))  | (1 << (4 * 2)) | (1 << (5 * 2)) | (1 << (7 * 2)) | (1 << (8 * 2)) | (1 << (9 * 2)) | (1 << (10 * 2)) | (1 << (11 * 2)) | (1 << (13 * 2)) | (1 << (14 * 2)) | (1 << (15 * 2));
		
		// the SPEEDER register is irrelevant
		
	}
	
	// GPIOE
	{
	
		// set the required pins as gerneral purpose output
		GPIOE->MODER &= ~((3 << (3 * 2)) | (3 << (7 * 2)) | (3 << (8 * 2)) | (3 << (9 * 2)) | (3 << (10 * 2)) | (3 << (11 * 2)) | (3 << (12 * 2)) | (3 << (13 * 2)) | (3 << (14 * 2)) | (3 << (15 * 2)));
		GPIOE->MODER |= (1 << (3 * 2)) | (1 << (7 * 2)) | (1 << (8 * 2)) | (1 << (9 * 2)) | (1 << (10 * 2)) | (1 << (11 * 2)) | (1 << (12 * 2)) | (1 << (13 * 2)) | (1 << (14 * 2)) | (1 << (15 * 2));
		
		// no need to set the OTYPER register, as it is set to push/pull on reset
		
		// enable pull up resistors for the required pins
		GPIOE->PUPDR &= ~((3 << (3 * 2)) | (3 << (7 * 2)) | (3 << (8 * 2)) | (3 << (9 * 2)) | (3 << (10 * 2)) | (3 << (11 * 2)) | (3 << (12 * 2)) | (3 << (13 * 2)) | (3 << (14 * 2)) | (3 << (15 * 2)));
		GPIOE->PUPDR |= (1 << (3 * 2)) | (1 << (7 * 2)) | (1 << (8 * 2)) | (1 << (9 * 2)) | (1 << (10 * 2)) | (1 << (11 * 2)) | (1 << (12 * 2)) | (1 << (13 * 2)) | (1 << (14 * 2)) | (1 << (15 * 2));
		
		// the SPEEDER register is irrelevant
		
	}
	
	// set the display for reset
	GPIOD->ODR |= (1 << 7);
	GPIOD->ODR |= (1 << 11);
	
	GPIOD->ODR |= (1 << 4);
	GPIOD->ODR |= (1 << 5);
	
	// send the reset impulse
	GPIOD->ODR &= ~(1 << 3);
	u_delay(15);
	GPIOD->ODR |= (1 << 3);
	
	// start the initialization
	LCD_WriteReg(0x0010, 0x0001); // Enter sleep mode
	LCD_WriteReg(0x001E, 0x00B2); // Set initial power parameters.
	LCD_WriteReg(0x0028, 0x0006); // Set initial power parameters.
	LCD_WriteReg(0x0000, 0x0001); // Start the oscillator.
	LCD_WriteReg(0x0001, 0x72EF); // Set pixel format and basic display orientation
	LCD_WriteReg(0x0002, 0x0600); // enable correct LCD driving mode
	LCD_WriteReg(0x0010, 0x0000); // Exit sleep mode.
	
	// wait 30ms
	u_delay(30000);
	
	LCD_WriteReg(0x0011, 0x6870); // Configure pixel color format and MCU interface parameters.
	LCD_WriteReg(0x0012, 0x0999); // Set analog parameters
	LCD_WriteReg(0x0026, 0x3800);
	LCD_WriteReg(0x0007, 0x0033); // Enable the display
	LCD_WriteReg(0x000C, 0x0005); // Set VCIX2 voltage to 6.1V.
	LCD_WriteReg(0x000D, 0x000A); // Configure Vlcd63 and VCOMl
	LCD_WriteReg(0x000E, 0x2E00);
	LCD_WriteReg(0x0044, (240-1) << 8); // Set the display size and ensure that the GRAM windowis set to allow access to the full display buffer.
	LCD_WriteReg(0x0045, 0x0000);
	LCD_WriteReg(0x0046, 320-1);
	LCD_WriteReg(0x004E, 0x0000); // Set cursor to 0,0
	LCD_WriteReg(0x004F, 0x0000);
	
	// enable backlight
	GPIOD->ODR |= (1 << 13);
	
	// enable timer timer 6
	{
	
	// initialize the time
	ms = 0;
	
	// peripheral enable
	RCC->APB1ENR |= RCC_APB1ENR_TIM6EN;
	
	// enable interrupts
	TIM6->DIER |= (1 << 0);
	
	// set the prescaler and the auto reload register
	TIM6->PSC = 84 - 1;
	TIM6->ARR = DISPLAY_ARR;
	
	// enable the interrupt in the NVIC and set priority
	NVIC_SetPriority(TIM6_DAC_IRQn, 4);
	NVIC_EnableIRQ(TIM6_DAC_IRQn);
	
}
	
}

// initialize the GPIO ports and pins
void MatrixInit() {
	
	// enable GPIO A and B ports
	RCC->AHB1ENR |= RCC_AHB1ENR_GPIOAEN | RCC_AHB1ENR_GPIOBEN;
	
	// GPIOA
	{
	
		// leave all pins as an input
		
		// no need to set the OTYPER register as we only have inputs
		
		// enable pull down resistors for the required pins
		GPIOA->PUPDR &= ~((3 << (3 * 2)) | (3 << (4 * 2)) | (3 << (5 * 2)) | (3 << (6 * 2)));
		GPIOA->PUPDR |= (1 << (3 * 2)) | (1 << (4 * 2)) | (1 << (5 * 2)) | (1 << (6 * 2));
		
		// the SPEEDER register is irrelevant
		
	}
	
	// GPIOB
	{
	
		// set the required pins as gerneral purpose output
		GPIOB->MODER &= ~((3 << (4 * 2)) | (3 << (5 * 2)) | (3 << (6 * 2)) | (3 << (7 * 2)));
		GPIOB->MODER |= (1 << (4 * 2)) | (1 << (5 * 2)) | (1 << (6 * 2)) | (1 << (7 * 2));
		
		// set the B ports to open drain
		GPIOB->OTYPER |= (1 << 4) | (1 << 5) | (1 << 6) | (1 << 7);
		
		// do not use pull up or down resistors
		
		// the SPEEDER register is irrelevant
		
		GPIOB->ODR |= (1 << 4) | (1 << 5) | (1 << 6) | (1 << 7);
		
	}
	
}

// initialize the timer 7
void TimerInit() {
	
	// initialize the time
	ms = 0;
	
	// peripheral enable
	RCC->APB1ENR |= RCC_APB1ENR_TIM7EN;
	
	// enable the counter
	TIM7->CR1 |= (1 << 0);
	
	// enable interrupts
	TIM7->DIER |= (1 << 0);
	
	// set the prescaler and the auto reload register
	TIM7->PSC = 84 - 1;
	TIM7->ARR = 1000 - 1;
	
	// enable the interrupt in the NVIC and set priority
	NVIC_SetPriority(TIM7_IRQn, 4);
	NVIC_EnableIRQ(TIM7_IRQn);
	
}

// initialize the timer 12
static void FrequencyInit() {

	// enable GPIO B and H ports
	RCC->AHB1ENR |= RCC_AHB1ENR_GPIOBEN | RCC_AHB1ENR_GPIOHEN;
	
	// GPIOB
	{
	
		// set the required pins as an alternate function
		GPIOB->MODER &= ~(3 << (14 * 2));
		GPIOB->MODER |= (2 << (14 * 2));
		
		// set the required pins as an alternate function to forward to timer 12
		GPIOB->AFR[1] &= ~(15 << ((14 - 8) * 4));
		GPIOB->AFR[1] |= (9 << ((14 - 8) * 4));
		
		// do not use pull up or down resistors
		
		// the SPEEDER register is irrelevant
		
		GPIOB->ODR |= (1 << 4) | (1 << 5) | (1 << 6) | (1 << 7);
		
	}

	// peripheral enable
	RCC->APB1ENR |= RCC_APB1ENR_TIM12EN;
	
	// enable the counter
	TIM12->CR1 |= (1 << 0);
	
	// configure channel 1 as input
	TIM12->CCMR1 |= (1 << 0);
	
	// configure capture mode for rising edge detection
	TIM12->CCER &= ~((1 << 1) | (1 << 3));
	TIM12->CCER |= (1 << 0);
	
}

// cleanup frequency
void FrequencyDeinit() {
	
	// disable interrupts
	TIM12->DIER &= ~(1 << 1);
}

// initialize the timer 12 for capture
void CaptureInit() {
	
	frequencyMode = CAPTURE_MODE;
	
	FrequencyInit();
	
	// enable interrupts for capture events
	TIM12->DIER = (1 << 1);
	
	// set capture mode
	TIM12->SMCR = 0;
	
	// set the prescaler and auto reload register
	TIM12->PSC = CAPTURE_PRESCALER - 1;
	TIM12->ARR = 0xFFFF;
	
	// enable the interrupt in the NVIC and set priority
	NVIC_SetPriority(TIM8_BRK_TIM12_IRQn, 4);
	NVIC_EnableIRQ(TIM8_BRK_TIM12_IRQn);

}

// initialize the timer 12 for counting
void CounterInit() {
	
	frequencyMode = COUNTER_MODE;
	
	FrequencyInit();
	
	// disable interrupts
	TIM12->DIER = 0;
	
	// select channel 1 for counter input
	TIM12->SMCR = (7 << 0) | (5 << 4);
	
	// set the prescaler
	TIM12->PSC = COUNTER_PRESCALER - 1;
	
	// dsiable the the interrupt in the NVIC
	NVIC_DisableIRQ(TIM8_BRK_TIM12_IRQn);

}

// initialize the LIN bus
void BusInit() {
	
	// enable GPIO B and C ports
	RCC->AHB1ENR |= RCC_AHB1ENR_GPIOBEN | RCC_AHB1ENR_GPIOCEN;
	
	// enable USART6
	RCC->APB2ENR |= RCC_APB2ENR_USART6EN;
	
	// GPIOC
	{
	
		// set the required pins as gerneral purpose output
		GPIOC->MODER &= ~(3 << (6 * 2));
		GPIOC->MODER |= (1 << (6 * 2));
		
		// do not use pull up or down resistors
		
		// the SPEEDER register is irrelevant
		
		GPIOC->ODR |= (1 << 6);
		
	}
	
	// GPIOB
	{
	
		// set the required pins as gerneral purpose output
		GPIOB->MODER &= ~(3 << (2 * 2));
		GPIOB->MODER |= (1 << (2 * 2));
		
		// do not use pull up or down resistors
		
		// the SPEEDER register is irrelevant
		
		// activate the transciever
		GPIOB->ODR &= ~(1 << 2);
		u_delay(100);
		GPIOB->ODR |= (1 << 2);
		
	}
	
	// GPIOC
	{
		
		GPIOC->ODR &= ~(1 << 6);
	
		// set the required pins as an alternate function
		GPIOC->MODER &= ~((3 << (6 * 2)) | (3 << (7 * 2)));
		GPIOC->MODER |= (2 << (6 * 2)) | (2 << (7 * 2));
		
		// set the required pins as an alternate function to forward to USART6
		GPIOC->AFR[0] &= ~((15 << (6 * 4)) |(15 << (7 * 4)));
		GPIOC->AFR[0] |= (8 << (6 * 4)) | (8 << (7 * 4));
		
		// do not use pull up or down resistors
		
		// the SPEEDER register is irrelevant
		
	}
	
	// set baud rate
	USART6->BRR = 0x00001117;
	
	// enable the reciever, transmitter, USART and the RXNE / TC interrupts
	USART6->CR1 |= (1 << 2) |(1 << 3) | (1 << 5) | (1 << 6) | (1 << 13);
	
	// enable the LIN mode and LIN break interrupt
	USART6->CR2 |= (1 << 6) | (1 << 14);
	
	// enable the interrupt in the NVIC and set priority
	NVIC_SetPriority(USART6_IRQn, 16);
	NVIC_EnableIRQ(USART6_IRQn);
}

// initialize teh ADC and DMA
void ADCInit() {
	
	// enable the interrupt in the NVIC and set priority
	NVIC_SetPriority(DMA2_Stream0_IRQn, 24);
	NVIC_EnableIRQ(DMA2_Stream0_IRQn);
	
	// enable DMA perihperal
	RCC->AHB1ENR |= RCC_AHB1ENR_DMA2EN;
	
	// disable DMA
	DMA2_Stream0->CR &= ~(1 << 0);
	
	// set peripheral port address
	DMA2_Stream0->PAR = &ADC1->DR;
	
	// set number of items to be transferred
	DMA2_Stream0->NDTR = 4;
	
	// select channel
	DMA2_Stream0->CR |= (0 << 25);
	
	// set TC interrupt enable, circular mode, memory increment, the peripheral as the flow controller, data size and priority
	DMA2_Stream0->CR |= (1 << 4) | (1 << 8) | (1 << 10) | (1 << 11) | (1 << 13) | (3 << 16);
	
	// enable DMA
	DMA2_Stream0->CR |= (1 << 0);
	
	// enable ADC peripheral
	RCC->APB2ENR |= RCC_APB2ENR_ADC1EN;
	
	// enable GPIO peripheral
	RCC->AHB1ENR |= RCC_AHB1ENR_GPIOCEN;
	
	GPIOC->MODER |= (3 << (2 * 0)) | (3 << (2 * 1));
	
	// enable scan mode
	ADC1->CR1 |= (1 << 8);
	
}

// delay the execution in us
void u_delay(uint32_t us) {

	for (uint32_t i = 0; i < us * WAIT_MULTIPLIER; i++) {
	
		// do not compute an operation
		__NOP();
	}
	
}

// get the state of the user button
bool getButton() {
	
	// return the state of the button input
	return (bool) (GPIOA->IDR & (1 << 0));
}

// set the LEDs from a bitmask
void LEDs_Write(uint16_t data) {

	// enable writing to IC3
	GPIOD->ODR |= (1 << 11);
	GPIOD->ODR &= ~(1 << 7);
	
	LCD_Output16BitWord(data);

	// update the IC3 latches
	GPIOD->ODR |= (1 << 5);
	GPIOD->ODR &= ~(1 << 5);
	
	GPIOD->ODR |= (1 << 5);
	
	// disable writing to IC3
	GPIOD->ODR |= (1 << 7);
	GPIOD->ODR &= ~(1 << 11);
	
}

// reorder bits to the registers
static inline void LCD_Output16BitWord(uint16_t data) {
	
	// shift accordingly
	dataToReg(&data, &GPIOD->ODR, 0, 14, 2);
  dataToReg(&data, &GPIOD->ODR, 2, 0, 2);
  dataToReg(&data, &GPIOD->ODR, 13, 8, 3);
  dataToReg(&data, &GPIOE->ODR, 4, 7, 9);
}

// send a command to the LCD display
static inline void LCD_WriteCommand(uint16_t command) {
	
	// set the display for a command
	GPIOE->ODR &= ~(1 << 3);
	
	// enable writing to the display
	GPIOD->ODR &= ~((1 << 11) | (1 << 7));
	GPIOD->ODR &= ~(1 << 5);
	
	LCD_Output16BitWord(command);
	
	// disable writing to the display and end the write cycle
	GPIOD->ODR |= (1 << 5) | (1 << 11) | (1 << 7);
	
}

// send data to the LCD display
static inline void LCD_WriteData(uint16_t data) {
	
	// set the display for data
	GPIOE->ODR |= (1 << 3);
	
	// enable writing to the display
	GPIOD->ODR &= ~((1 << 11) | (1 << 7));
	GPIOD->ODR &= ~(1 << 5);
	
	LCD_Output16BitWord(data);
	
	// disable writing to the display and end the write cycle
	GPIOD->ODR |= (1 << 5) | (1 << 11) | (1 << 7);
}

// send data to the LCD display
static inline void LCD_WriteRaw(uint16_t dataD, uint16_t dataE) {
	
	// enable writing to the display
	GPIOD->ODR &= ~((1 << 0) | (1 << 1) | (1 << 5) | (1 << 7) | (1 << 8) | (1 << 9) | (1 << 10) | (1 << 11) | (1 << 14) | (1 << 15));
	GPIOE->ODR &= ~((1 << 7) | (1 << 8) | (1 << 9) | (1 << 10) | (1 << 11) | (1 << 12) | (1 << 13) | (1 << 14) | (1 << 15));
	
	GPIOD->ODR |= dataD;
	GPIOE->ODR |= dataE;
	
	// disable writing to the display and end the write cycle
	GPIOD->ODR |= (1 << 5) | (1 << 11) | (1 << 7);
}

// send data to the LCD display
static inline void LCD_WriteReg(uint16_t command, uint16_t data) {

	LCD_WriteCommand(command);
	LCD_WriteData(data);
	
}

// set the cursor to the desired position
void LCD_SetCursor(uint32_t x, uint32_t y) {

	LCD_WriteReg(0x004E, x);
	LCD_WriteReg(0x004F, y);
	
}

// color in the pixel at the cursor position
void LCD_DrawPixel(uint16_t color) {
	
	LCD_WriteReg(0x0022, color);

}

// fill the display with a single color
void LCD_ClearDisplay(uint16_t color) {
	
	LCD_DrawPixel(color);
	
	// enable writing to the display
	GPIOD->ODR &= ~((1 << 11) | (1 << 7));
	
	uint16_t cacheEnabled = GPIOD->ODR | (1 << 5);
	uint16_t cacheDisabled = cacheEnabled & ~(1 << 5);
	
#ifdef RAM_TARGET
	
	for (uint32_t i = 0; i < DISPLAY_SIZE_X * DISPLAY_SIZE_Y; i++) {
			
		// toggle the 5 pin
		GPIOD->ODR = cacheDisabled;
		
		// wait a little
		__NOP();
		__NOP();
		__NOP();
		__NOP();
		
		// toggle the 5 pin
		GPIOD->ODR = cacheEnabled;
	}
	
#else
	
	// initialize the exit flag
	clearDisplayExitFlag = false;
	
	// enable the timer 6 counter in one pulse mode
	TIM6->CNT = 0;
	TIM6->CR1 = (1 << 0) | (1 << 3);
	
	while (!clearDisplayExitFlag) {
			
		// toggle the 5 pin
		GPIOD->ODR = cacheDisabled;
		
		// wait a little
		__NOP();
		__NOP();
		__NOP();
		__NOP();
		
		// toggle the 5 pin
		GPIOD->ODR = cacheEnabled;
		
	}
	
#endif
	
	// disable writing to the display
	GPIOD->ODR |= (1 << 11) | (1 << 7);

}

// write a letter to the screen
void LCD_WriteLetter(uint32_t x, uint32_t y, uint16_t colorfg, uint16_t colorbg, uint8_t c) {
	
	uint16_t cacheDfg, cacheEfg, cacheDbg, cacheEbg;
	
	// shift accordingly
	dataToReg(&colorfg, &cacheDfg, 0, 14, 2);
  dataToReg(&colorfg, &cacheDfg, 2, 0, 2);
  dataToReg(&colorfg, &cacheDfg, 13, 8, 3);
  dataToReg(&colorfg, &cacheEfg, 4, 7, 9);
	dataToReg(&colorbg, &cacheDbg, 0, 14, 2);
  dataToReg(&colorbg, &cacheDbg, 2, 0, 2);
  dataToReg(&colorbg, &cacheDbg, 13, 8, 3);
  dataToReg(&colorbg, &cacheEbg, 4, 7, 9);
	
	// set the display for data
	cacheDfg |= (1 << 3);
	cacheDbg |= (1 << 3);
	
  // use a pointer to the font array as an index
  for (uint16_t* i = ((uint16_t*) console_font_12x16 + (16 * (uint16_t) c)); i < (uint16_t*) console_font_12x16 + (16 * (uint16_t) c) + 16; i++) {
	
		LCD_SetCursor(x, y);
		LCD_WriteCommand(0x0022);
		
		// set the display for data
		GPIOE->ODR |= (1 << 3);

    // iterate through the bits of 2 chars (1 lin = 12 bits)
    for (uint8_t j = LETTER_Y - 1; j >= (LETTER_Y - LETTER_X - 1); j--) {

      // apply a bit mask to the char to read the bit we are looking for
      if ((((uint16_t) 1) << j) & *i) {

        // draw foreground if the bit is 1
				LCD_WriteRaw(cacheDfg, cacheEfg);
      } else{
			
				// draw background if the bit is 0
				LCD_WriteRaw(cacheDbg, cacheEbg);
			}
    }
		
		// we have reached the end of the line
		y++;
  } 
}

// write a string to the screen
void LCD_WriteString(uint32_t x, uint32_t y, uint16_t colorfg, uint16_t colorbg, uint8_t *c) {

	// iterate through the string
	while (*c != '\0') {
	
		// call the write letter function
		LCD_WriteLetter(x, y, colorfg, colorbg, *c);
		
		// increment the x position
		x += FONT_X;
		
		// handle line breaks
		if (x > DISPLAY_SIZE_X - LETTER_X) {
			
			// reset the x position and increment the y position
			x = 0;
			y += LETTER_Y;
			
			// handle page breaks
			if (y > DISPLAY_SIZE_Y - LETTER_Y) {
				
				// reset the y position
				y = 0;
			}
		}
		
		// increment the char pointer (string)
		c++;
	}

}

// read from the matrix
uint16_t MatrixRead() {
	
	uint16_t ret = 0;
	
	// check the input for every row
	for (uint8_t i = 0; i <= 3; i++) {
		
		// set the line input
		GPIOB->ODR |= (1 << 4) | (1 << 5) | (1 << 6) | (1 << 7);
		GPIOB->ODR &= ~(1 << (i + 4));
		
		// delay to avoid problems
		u_delay(30);
		
		// store the state
		dataToReg(&GPIOA->IDR, &ret, 3, 4 * i, 4);
		
	}
		
	return ~ret;
}

// read values from the ADC
void ADCRead(uint16_t *result) {
	
	adcExitFlag = false;
	
	// disable DMA
	DMA2_Stream0->CR &= ~(1 << 0);
	
	// set memory port adress
	DMA2_Stream0->M0AR = result;
	
	// enable DMA
	DMA2_Stream0->CR |= (1 << 0);
	
	// enable ADC 1 
	ADC1->CR2 |= (1 << 0);
	
	// enable channels
	ADC1->SQR3 &= ~(((1 << 24) - 1) ^ ((1 << 20) - 1));
	ADC1->SQR1 |= ((4 - 1) << 20);
	ADC1->SQR3 &= ~((1 << 21) - 1);
	ADC1->SQR3 |= (10 << 0) | (11 << 5) | (14 << 10) | (15 << 15);
	
	// enable DMA and continous mode and start the conversion
	ADC1->CR2 |= (1 << 1) | (1 << 8) | (1 << 30);
	
	while (!adcExitFlag) {
		
		// wait until the values are read
		__WFI();
	}
}

// ISR for timer 6
void TIM6_DAC_IRQHandler(void) {
	TIM6->SR = 0;
	
	// set the exit flag
	clearDisplayExitFlag = true;
}

// ISR for timer 7
void TIM7_IRQHandler(void) {
	TIM7->SR = 0;

	// increment the milliseconds
	ms++;
	
	if ((frequencyMode == COUNTER_MODE) && (ms % COUNTER_INTERVAL == 0)) {
		
		// update the frequency when in counter mode
		frequency = (TIM12->CNT * 1000) / (uint32_t) (COUNTER_INTERVAL * COUNTER_CORRECTION);
		TIM12->CNT = 0;
	}
}



// ISR for timer 12
void TIM8_BRK_TIM12_IRQHandler(void) {
	
	if (frequencyMode == CAPTURE_MODE) {
	
		// update the fruequency ehen in capture mode
		static uint16_t lastTime = 0;
		
		TIM12->SR = 0;
		
		// store the frequency
		uint16_t deltat = TIM12->CCR1 - lastTime;
		frequency = BASE_FREQUENCY / CAPTURE_PRESCALER * CAPTURE_CORRECTION / deltat;
		
		// store the last time
		lastTime = TIM12->CCR1;
	}
}

// ISR for USART6
void USART6_IRQHandler(void) {
	
	static enum {
		IDLE,
		LBD,
		ADDRESS,
		SEND
	} LINstate = IDLE;
	
	static uint8_t LINsize;
	static uint8_t LINremaining;
	static uint8_t LINchecksum;
	static uint32_t LINbuffer;
	
	uint32_t status = USART6->SR;
	uint8_t data = USART6->DR;
	
	// unset flags
	USART6->SR = 0;
	
	switch (LINstate) {
	
		case IDLE: {
			
			// detect LBD
			if (status & (1 << 8)) {
			
				LINstate = LBD;
			}
		} break;
		
		case LBD: {
			
			// only act on recieve interrupt
			if (status & (1 << 5)) {
			
				// detect the sync field
				if (data == 0x55) {
				
					LINstate = ADDRESS;
				} else {
				
					LINstate = IDLE;
				}
			}
		} break;
		
		case ADDRESS: {
			
			// only act on recieve interrupt
			if (status & (1 << 5)) {
			
				// detect the own LIN address
				if ((data & 0x0F) == LIN_ADDRESS) {
					
					switch (data & 0xF0) {
					
						case (1 << 4): {
							
							// TEMP
							LINbuffer = ms;
							LINsize = 2 - 1;
							
						} break;
						
						case (2 << 4): {
							
							// FREQ
							LINbuffer = frequency;
							LINsize = 4 - 1;
							
						} break;
						
						case (3 << 4): {
							
							// KEYS
							LINbuffer = keys >> 8;
							LINbuffer |= (keys & 0xFF) << 8;
							LINsize = 2 - 1;
							
						} break;
						
					}
					
					LINremaining = 0;
					
					// calculate checksum
					LINchecksum = checksum((uint8_t *) &LINbuffer, LINsize, data);
					
					// send the first byte
					USART6->DR = ((uint8_t *) &LINbuffer)[LINremaining];
				
					LINstate = SEND;
				} else {
				
					LINstate = IDLE;
				}
			}
		
		} break;
		
		case SEND: {
			
			if (status & (1 << 6)) {
	
				// TC interrupt
				
				if (LINremaining != LINsize) {
					
					// send a byte
					LINremaining++;
					USART6->DR = ((uint8_t *) &LINbuffer)[LINremaining];
				} else {
					
					// send checksum
					USART6->DR = LINchecksum;
					
					// end transmission
					LINstate = IDLE;
				}
			}
		} break;
	
	}
}
 
// ISR for DMA
void DMA2_Stream0_IRQHandler(void) {
	
	// check for an transfer complete
	if (DMA2->LISR & (1 << 5)) {
		
		// clear the flags
		DMA2->LIFCR |= (1 << 4) | (1 << 5);
	
		// stop DMA requests
		ADC1->CR2 &= ~(1 << 8);
		
		// set the exit flag
		adcExitFlag = true;
	}
}
