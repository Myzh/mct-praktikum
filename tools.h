#ifndef TOOLS_H
#define TOOLS_H

#define WAIT_MULTIPLIER 7
#define DISPLAY_SIZE_X 320
#define DISPLAY_SIZE_Y 240
#define LETTER_X 12
#define LETTER_Y 16
#define LIN_ADDRESS 6


// include general headers
#include <stdbool.h>

// include local headers
#include "STM32F4xx.h"
#include "_mcpr_stm32f407.h"


extern volatile uint32_t ms;
extern volatile uint32_t frequency;
extern volatile uint16_t keys;

void LCDInit();
void MatrixInit();
void TimerInit();
void FrequencyDeinit();
void CaptureInit();
void CounterInit();
void BusInit();
void ADCInit();

void u_delay(uint32_t us);

bool getButton();

void LEDs_Write(uint16_t data);

void LCD_SetCursor(uint32_t x, uint32_t y);
void LCD_DrawPixel(uint16_t color);
void LCD_ClearDisplay(uint16_t color);
void LCD_WriteLetter(uint32_t x, uint32_t y, uint16_t colorfg, uint16_t colorbg, uint8_t c);
void LCD_WriteString(uint32_t x, uint32_t y, uint16_t colorfg, uint16_t colorbg, uint8_t *c);

uint16_t MatrixRead();

void ADCRead(uint16_t *result);

#endif
