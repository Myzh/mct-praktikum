#include "main.h"


// main function
int32_t main() {

	mcpr_SetSystemCoreClock(); // first line of main
	
	uint16_t colorbg = 0xFFFF;
	uint16_t colorfg = 0x0000;
	uint16_t lastInput = 0;
	
	/*
	// setup the ports for GPIO
	LCDInit();
	
	// loop indefinitely
	uint16_t colorbg = 0xF800; // Rot
	uint16_t colorfg;
	while(1) {
		GPIOD->ODR ^= 0x1000;// toggle Pin PD12 (gr�ne LED)
		LCD_ClearDisplay(colorbg);// l�sche das Display
		// gib einen Text aus
		LCD_WriteString( 220, 220, colorfg, colorbg, "Hallo Welt!" );
		// Wechsle Farbe
		colorfg = colorbg;
		colorbg = ~colorbg;
	}
	*/
	
	// setup the ports for GPIO
	LCDInit();
	MatrixInit();
	TimerInit();
	BusInit();
	ADCInit();
	
	LCD_ClearDisplay(colorbg);
	
	
	while (true) {
		
		// read the key state and ADC values
		keys = MatrixRead();
		uint16_t values[4];
		ADCRead(values);
		
		char number[24];
		
		sprintf(number, "ADC 0: %010d", values[0]);
		LCD_WriteString(0, 0, colorfg, colorbg, number);
		
		sprintf(number, "ADC 1: %010d", values[1]);
		LCD_WriteString(0, 0, colorfg, colorbg, number);
		
		uint32_t startTime = ms;
		
		CounterInit();
		while (ms - startTime < 50);
		FrequencyDeinit();
		
		sprintf(number, "Counter: %010d", frequency);
		LCD_WriteString(0, 32, colorfg, colorbg, number);
		
		CaptureInit();
		while (ms - startTime < 100);
		FrequencyDeinit();
		
		sprintf(number, "Capture: %010d", frequency);
		LCD_WriteString(0, 48, colorfg, colorbg, number);
		
	}

}
